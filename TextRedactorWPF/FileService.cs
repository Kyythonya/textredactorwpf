﻿using System;
using System.IO;
using System.Windows.Documents;

namespace TextRedactorWPF
{
    public class FileService
    {
        public string FilePath { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + @"\Text.rtf";
        public bool IsFileSaved { get; set; } = false;

        public void OpenFile(TextRange text)
        {
            using (var stream = new FileStream(FilePath, FileMode.Open))
            {
                text.Load(stream, System.Windows.DataFormats.Rtf);
            }
        }

        public void SaveFile(TextRange text)
        {
            using (var stream = new FileStream(FilePath, FileMode.OpenOrCreate))
            {
                text.Save(stream, System.Windows.DataFormats.Rtf);
            }

            IsFileSaved = true;
        }
    }
}
