﻿using System.Windows;
using System.Windows.Controls.Ribbon;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;

namespace TextRedactorWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        private FileService _fileService;

        public MainWindow()
        {
            InitializeComponent();

            _fileService = new FileService();

            fontFamilyComboBox.ItemsSource = Fonts.SystemFontFamilies;
            fontSizeComboBox.ItemsSource = new double[] { 3.0, 4.0, 5.0, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5,
                9.0, 9.5, 10.0, 10.5, 11.0, 11.5, 12.0, 12.5, 13.0, 13.5, 14.0, 15.0, 16.0, 17.0, 18.0,
                19.0, 20.0, 22.0, 24.0, 26.0, 28.0, 30.0, 32.0, 34.0, 36.0, 38.0, 40.0, 44.0, 48.0, 52.0, 56.0 };

            fontSizeComboBox.SelectedItem = textEntries.FontSize = 14;
            fontFamilyComboBox.SelectedItem = textEntries.FontFamily = new FontFamily("Arial");
        }

        private TextRange GetText()
        {
            return new TextRange(textEntries.Document.ContentStart, textEntries.Document.ContentEnd);
        }

        private void CreateNewFileClick(object sender, RoutedEventArgs e)
        {
            var result = System.Windows.Forms.MessageBox.Show(
                   "Do you want to save a file?",
                   "WordPad",
                   MessageBoxButtons.YesNoCancel,
                   MessageBoxIcon.None,
                   MessageBoxDefaultButton.Button1);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                _fileService.SaveFile(GetText());
            }
            else if (result == System.Windows.Forms.DialogResult.Cancel) return;

            textEntries.Document.Blocks.Clear();
        }

        private void OpenFileClick(object sender, RoutedEventArgs e)
        {
            CreateNewFileClick(sender, e);

            var openFileDialog = new OpenFileDialog
            {
                Filter = "Document files (*.rtf)|*.rtf"
            };

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _fileService.FilePath = openFileDialog.FileName;
                _fileService.OpenFile(GetText());
            }
        }

        private void SaveFileClick(object sender, RoutedEventArgs e)
        {
            _fileService.SaveFile(GetText());
        }

        private void SaveAsFileClick(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                FileName = "Document.rtf",
                Filter = "Document files (*.rtf)|*.rtf"
            };

            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _fileService.FilePath = saveFileDialog.FileName;
                _fileService.SaveFile(GetText());
            }
        }

        private void OpenAboutDialogClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Smth about word.");
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            int timerIntervalSeconds = 10;

            var timer = new DispatcherTimer
            {
                Interval = new System.TimeSpan(0, 0, timerIntervalSeconds)
            };
            timer.Tick += SaveFileTimerTick;
            timer.Start();
        }

        private void SaveFileTimerTick(object sender, System.EventArgs e)
        {
            _fileService.SaveFile(GetText());
        }

        private void ApplyStyleToSelectedText(DependencyProperty fontProperty, object fontValue)
        {
            if (fontValue != null)
                textEntries.Selection.ApplyPropertyValue(fontProperty, fontValue);
        }

        private void FontFamilyComboBoxSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedFontFamily = (FontFamily)e.AddedItems[0];
            ApplyStyleToSelectedText(TextElement.FontFamilyProperty, selectedFontFamily);
        }

        private void FontSizeComboBoxSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            ApplyStyleToSelectedText(TextElement.FontSizeProperty, e.AddedItems[0]);
        }
    }
}
